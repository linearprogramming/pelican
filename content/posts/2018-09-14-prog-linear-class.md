Title: Disciplina de Análise de Sistemas e Programação Linear.
Date: 2018-09-14 13:00
Category: Início
Tags: programacao, linear, deha, ufc
Slug: disciplina-de-programacao-linear

# Caracterização:

* Subitem da programação matemática;
* Modelos utilizados em pesquisa operacional; 
* Modelo de otimização;
* Objetivo: "Alocar recursos escassos a atividades em concorrência" 

![Alt Text]({filename}../images/img.png){:height="395px" width="375px"}
![Alt Text]({filename}../images/img.png){:height="395px" width="375px"}

# Exemplos:

## Exemplo 1:

* Uma empresa pode fabricar dois produtos ( 1 e 2 );
* Na fabricação do produto 1 a empresa gasta nove horas-homem e três horas-máquina (a tecnologia utilizada é intensiva em mão-de-obra);
* Na fabricação do produto 2 a empresa gasta uma hora-homem e uma hora-máquina (a tecnologia é intensiva em capital);
* A empresa dispõe de 18 horas-homem e 12 horas-máquina para um período de produção;
* Sabe-se que os lucros líquidos dos produtos são $4 e $1 respectivamente.

## Pergunta-se:

* Quanto a empresa deve fabricar de cada produto para ter o maior lucro?
* Caso se obtenha algum recurso financeiro externo, para investimento em expansão, em quais dos recursos a empresa deveria aplicá-lo?
* Qual seria o impacto no lucro se alguns trabalhadores faltassem ao trabalho limitando as horas homens disponíveis em 15 horas?
* Sabendo-se que 4 máquinas são responsáveis pela produção no período em análise, até quanto se deveria pagar pelo aluguel de uma máquina se eventualmente uma das quatro máquinas quebrassem?

## Exemplo 2: Sistemas Ambientais

- FIXME

# Resolvendo Intuitivamente

* Que modelo mental poderia ser usado?
* Como se poderia utilizar a intuição para responder as perguntas?
* Tente resolver o problema sem utilizar um modelo formal.

# Transformando os dados em expressões matemáticas

* As restrições:
    * Não se pode utilizar o que não se tem!
    * A quantidade utilizada deve ser menor ou igual a quantidade disponível;
    * As quantidades de fabricação devem ser não negativas.

```math
9x_{1}+x_2 \le 18
```
```math
3x_1+x_2 \le 12
```
```math
x_1 \ge 0
```
```math
x_2 \ge 0
```

# Resolvendo o modelo

* Solução Gráfica;
* Solução Matricial;
* Método Simplex;
* Solução Computacional.

## Exemplo

* Função Objetivo:
    * $`MAX = 350x_1 + 300x_2`$

* Sujeito a:
    * Fatores de Produção → Limitação
    * Bombas: $`x_1 + x_2 \le 200`$
    * Horas de Mão de Obra: $`9x_1 + 6x_2 \le 1.566`$
    * Canos (metros): $`12x_1 + 16x_2 \le 2.880`$

* Então:
    * $`x_1 + x_2 \le 200`$
    * $`9x_1 + 6x_2 \le 1.566`$
    * $`12x_1 + 16x_2 \le 2.880`$
    * $`x_1 \ge 0`$
    * $`x_2 \ge 0`$

# Solução Gráfica

- FIXME

* Como determinar a solução ótima:
    * A partir da visualização do encontro das curvas de nível com as retas de restrição;
    * A partir da comparação dos diversos pontos extremos, para escolher o maior (menor) valor.

* Sumário:
    * Desenhe uma ou mais curvas de nível da função objetivo e determine a direção na qual curvas paralelas resultam em aumentos no valor da função objetivo; 
    * Desenhe curvas paralelas na direção do crescimento até que a curva toque a área de soluções em um único ponto;
    * Encontre às coordenadas deste ponto; 
    * Identifique as coordenadas de todos os pontos extremos da área de soluções factíveis e calcule os respectivos valores da função objetivo.
    * O ponto com o maior valor da função- objetivo é a solução ótima.






